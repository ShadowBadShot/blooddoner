import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React,{useEffect} from 'react';
// import screens
import Splash from './Src/Login/Splash';
import LoginRegister from './Src/Login/LoginRegister';
import Login from './Src/Login/Login';
import Register from './Src/Register/Register';
import OtpScreen from './Src/Register/OtpScreen'
import Intro from './Src/Login/Intro'
import Home from './Src/Home/Home'
import Search from './Src/Search/Search'
import Profile from './Src/Profile/Profile'
import ChangePassword from './Src/Login/ChangePassword';

const Stack = createNativeStackNavigator();

const AppStack = (props) => {

  return (
    <NavigationContainer independent={true}>
      <Stack.Navigator
        initialRouteName="Splash"
        screenOptions={{headerShown: false, animationEnabled: false,}}
        options={{
          animationEnabled: true,
        }}>
        <Stack.Screen name="Splash" component={Splash} />
        <Stack.Screen name = 'LoginRegister' component={LoginRegister} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Intro" component={Intro} />
        <Stack.Screen name="Register" component={Register} />
        <Stack.Screen name="OtpScreen" component={OtpScreen} />
        <Stack.Screen name="Home" component={Home} />  
        <Stack.Screen name="Search" component={Search} />  
        <Stack.Screen name="Profile" component={Profile} />
        <Stack.Screen name="ChangePassword" component={ChangePassword} />   

      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AppStack;

import React,{useRef,useState,useEffect} from 'react'
import {View,Image,StatusBar,Text,TextInput,ScrollView,Pressable}  from 'react-native'
import styles from '../Utils/MainStyles'
import * as constant from '../Utils/Constant'
import  Header from '../Component/Header'
import CommonButton from '../Component/Button'
import {CommonActions} from '@react-navigation/native';
import {Phone,lock} from '../Utils/Images'
import { Dropdown } from 'react-native-element-dropdown';
import DropDown from '../Component/DropDown'
const data = [
  { label: 'Hissar', value: 'Hissar' },
  { label: 'Jind', value: 'Jind' },
  { label: 'Rohtak', value: 'Rohtak' },
  { label: 'Sirsa', value: 'Sirsa' },

];
const BloodData = [
  { label: 'A+', value: 'A+' },
  { label: 'A-', value: 'A-' },
  { label: 'B+', value: 'B+' },
  { label: 'B-', value: 'B-' },
  { label: 'O+', value: 'O+' },
  { label: 'O-', value: 'O-' },
  { label: 'AB+', value: 'AB+' },
  { label: 'AB-', value: 'AB-' }

];
const Profile =(props)=>{
const scrollRef = useRef(null)
const [phone,setPhone] = useState('')
const [pass,setPass] = useState('')
const [name,setName] = useState('')
const [city,setCity] = useState('')
const[bloodType,setBloodType] = useState('')

    const onLogin = ()=>{
    if(name==='') constant.showAlert("Please enter name",false)
    else if(phone === '') constant.showAlert("Please enter mobile number",false)
    else if(phone.length < 10) constant.showAlert("Please enter valid mobile number",false)
    else if(pass==='') constant.showAlert("Please enter password",false)
    else if(city==='') constant.showAlert('Please select city',false)
    else if(bloodType==='') constant.showAlert('Please select blood Type',false)
    else{
         props.navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [
                { name: 'OtpScreen' },
                
              ],
            }))
    }
       
    }
  


    return(
        <View style={{marginTop:StatusBar.currentHeight}}>
            <StatusBar backgroundColor={constant.baseColor} />
           <Header
          title="Profile"
          leftIcon={require("../Resource/Images/backIcon.png")}
          bgColor={constant.baseColor}
          leftIconPress={() => props.navigation.goBack()}
          rightExt={{tintColor:constant.whiteColor}}

          /> 
        
       <ScrollView ref={scrollRef}
            style={styles.loginSubView}>
           
<View >
      <Pressable style={styles.profileImageButton}>
       <Image 
       source={require('../Resource/Images/UserImage.png')}
       resizeMode='cover' style={styles.ProfileUserImage}
       />
       </Pressable>
       </View>
       <View style={{marginTop:constant.resWidth(0)}}>
           <TextInput 
           style={styles.registerTextInput}
            placeholder='Name'
            onChangeText={(text)=>{setName(text)}}
             ></TextInput>
      
           <TextInput 
           style={styles.registerTextInput}
            placeholder='Phone Number'
            editable={false}
            keyboardType='numeric'
            onChangeText={(text)=>{setPhone(text)}}
            maxLength={10}
             ></TextInput>

           
      
   <DropDown 
   placeholder='City'
   data={data}
   value={city}
   onSelect = {(text)=>{setCity(text)}}
   />

<DropDown 
   placeholder='Blood Type'
   data={BloodData}
   value={bloodType}
   onSelect = {(text)=>{setBloodType(text)}}
    dropDownStyleExt={{marginVertical:'5%' }}
   />
       
     <CommonButton 
     onPress={()=>{onLogin()}}
    title='Update'
     buttonStyleExt={{
    backgroundColor:constant.baseColor,
    width:'90%',
    alignSelf:'center',
    marginBottom:constant.resWidth(2),
    marginVertical:constant.resWidth(10)

     }}
     titleStyleExt={{
        color:constant.whiteColor,
        fontWeight:'600',
        fontSize:constant.font18
     }}
     />
</View>
 </ScrollView>
 </View>
    )
}

export default Profile
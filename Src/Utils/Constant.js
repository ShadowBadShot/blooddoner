import React from 'react';
import {Dimensions,ActivityIndicator,View,Text} from 'react-native';
import Snackbar from 'react-native-snackbar';

// import AsyncStorage from '@react-native-async-storage/async-storage';


export const deviceHeight = Dimensions.get('screen').height;
export const deviceWidth = Dimensions.get('screen').width;
export const resHeight = h => {
  return (deviceHeight * h) / 100;
};
export const resWidth = w => {
  return (deviceWidth * w) / 100;
};


export const homeGradiant=['#6E21E2','#8E89AF']
export const baseColor = '#D80032';
export const whiteColor = '#fff';
export const blackColor = '#000';
export const textColor = '#fff';
export const lightText = '#8c8c8c';
export const veryLightWhite = '#f2f2f2';
export const placeholderColor = '#cccccc';
export const introLightButton = '#808080';
export const lightBlackBg = '#595959';
export const darkBlackColor = '#333333'
export const blueColor = '#6E21E2';
export const yellowColor = '#F7B500';
export const orangeColor = '#FF4B00';
export const greenColor = '#00C075';
export const iconColor="#B0120C"
export const grayColor = '#808080'


// fonts
export const typeBold = 'Poppins-Bold';
export const typeExtraBold = 'Poppins-ExtraBold';
export const typeLight = 'Poppins-Light';
export const typeExtraLight = 'Poppins-ExtraLight';
export const typeMedium = 'Poppins-Medium';
export const typeRegular = 'Poppins-Regular';
export const typeSemiBold = 'Poppins-SemiBold';
export const typeThin = 'Poppins-Thin';

export const typeRobotoBold = 'Roboto-Bold';
export const typeRobotoLight = 'Roboto-Light';
export const typeRobotoMedium = 'Roboto-Medium';
export const typeRobotoRegular = 'Roboto-Regular';
export const typeRobotoThin = 'Roboto-Thin';

export const font = (deviceWidth * 2) / 100;
export const font2 = (deviceWidth * 2.3) / 100;
export const font4 = (deviceWidth * 2.6) / 100;
export const font5 = (deviceWidth * 2.9) / 100;
export const font6 = (deviceWidth * 3.2) / 100;
export const font8 = (deviceWidth * 3.5) / 100;
export const font10 = (deviceWidth * 3.8) / 100;
export const font12 = (deviceWidth * 4.1) / 100;
export const font14 = (deviceWidth * 4.4) / 100;
export const font16 = (deviceWidth * 4.7) / 100;
export const font18 = (deviceWidth * 5) / 100;
export const font20 = (deviceWidth * 5.3) / 100;
export const font22 = (deviceWidth * 5.6) / 100;
export const font24 = (deviceWidth * 5.9) / 100;
export const font26 = (deviceWidth * 6.2) / 100;
export const font28 = (deviceWidth * 6.5) / 100;
export const font30 = (deviceWidth * 6.8) / 100;
export const font32 = (deviceWidth * 7.1) / 100;
export const font34 = (deviceWidth * 7.4) / 100;
export const font36 = (deviceWidth * 7.7) / 100;

// utils
export const buttonRadius = resWidth(8);
export const borderRadius1 = resWidth(2);
export const borderRadius2 = resWidth(3);
export const borderRadius3 = resWidth(4);
export const borderRadius4 = resWidth(5);
export const borderRadius5 = resWidth(7);
export const sheetRadius = resWidth(10);
export const footerSpace = resWidth(2);
export const horizontalSpace = resWidth(6);

// statusbar
export const barColor = '#000';
export const barStyle = 'light-content';


export const showAlert = (message, text, snackAction, showTime) => {
  Snackbar.show({
    text: message,
    duration:
      showTime === undefined
        ? Snackbar.LENGTH_SHORT
        : type === 'long'
        ? Snackbar.LENGTH_LONG
        : type === 'infinte'
        ? Snackbar.LENGTH_INDEFINITE
        : Snackbar.LENGTH_SHORT,
    action: {
      text: text,
      onPress: snackAction,
    },
    backgroundColor: baseColor,
    // fontFamily: typeRegular,
    textColor: whiteColor,
  });
};





export const emptyLoader =(topspace)=>{
  return ( 
  <ActivityIndicator size='large' color={baseColor} style={{marginTop:topspace}} />)
}



// export const setAsyncData = async(login,data)=>{
//   try{
//     // await AsyncStorage.setItem(paramName,value) 
//     console.log("dataaa")
//     console.log('data'+login + "c:"+data)
//       AsyncStorage.multiSet([['islogin',login],['USER_DATA',JSON.stringify(data)]]);

//   } catch(e){
//     console.log("asyncError"+e)
//   }
// }



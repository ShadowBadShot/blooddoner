import React from 'react'
import {StyleSheet} from 'react-native'
import * as constant from '../Utils/Constant'

const styles = StyleSheet.create({
    LoginRegisterMainView:{
     flex:1,
     backgroundColor:constant.baseColor,
     alignItems:'center',
     justifyContent:'flex-end'
    },
    LoginRegiterImage:{
        width:constant.resWidth(60),
        height:constant.resWidth(60),
        tintColor:constant.whiteColor
    },
    

 //Intro
 IntroMainView:{
    flex:1,
    backgroundColor:constant.baseColor,
    alignItems:'center',
    justifyContent:'center'
 },
 introMainImage:{
    width:constant.resWidth(60),
    height:constant.resWidth(60),
    tintColor:constant.whiteColor
},
introSubView:{
  flexDirection:'row',
  width:'100%',
  justifyContent:'space-evenly',
  marginTop:constant.resWidth(22)
},
 IntroButton:{
    width:constant.resWidth(33),
    height:constant.resWidth(33),
    borderRadius:constant.resWidth(3),
    borderWidth:1,
    borderColor:constant.whiteColor,
    alignItems:'center',
    justifyContent:'center',
 },
 introImageStyle:{
    width:constant.resWidth(20),
    height:constant.resWidth(20),
    tintColor:'white'
 },
introTitle:{
color:constant.whiteColor,
fontWeight:'700',
fontSize:constant.font12,
marginTop:'4%'
},

    // Login

    LoginMainView:{
        flex:1,
        backgroundColor:constant.baseColor,
       
       },
       LoginImage:{
        width:constant.resWidth(80),
        height:constant.resWidth(50),
        alignSelf:'center',
        marginBottom:'0%',
        marginLeft:'5%',
        marginTop:constant.resWidth(35)
        
    },
    loginSubView:{
   backgroundColor:constant.whiteColor,

    },
   

    detailMainView:{
    flexDirection:'row',
    marginHorizontal:'5%',
    borderRadius:10,
    alignItems:'center',
    borderWidth:1.5,
    marginBottom:'5%',
    paddingVertical:'0.5%',
    borderColor:constant.grayColor
    } ,
    loginPhoneImage:{
        width:constant.resWidth(7),
        height:constant.resWidth(7),
        tintColor:constant.grayColor,
        marginHorizontal:"1.5%"
    },
    loginTextInput:{
  flex:1,
  fontSize:constant.font16,

    },
    loginBottomText:{
        fontSize:constant.font10, 
        alignSelf:'center',
        marginTop:'2%',
        color:constant.grayColor,
        marginBottom:'5%'

    },
    loginBottomText2:{
        fontSize:constant.font14, 
        color:constant.baseColor,
        fontWeight:'600'

    },
    loginForgetPassword:{
   alignSelf:'flex-end',
   marginRight:'5%',
   fontSize:constant.font14,
   fontWeight:'500',
   marginBottom:constant.resWidth(15),
   paddingVertical:'2%',
   color:constant.baseColor
    },
        // Register

        RegisterUserImage:{
            width:constant.resWidth(25),
            height:constant.resWidth(25),
            alignSelf:'center',
            marginBottom:'0%',
            marginLeft:'5%',
            marginVertical:constant.resWidth(15)
            
        },
    RegisterMainView:{
        flex:1,
        backgroundColor:constant.baseColor,
        alignItems:'center',
        justifyContent:'flex-end'
       },
      RegisterImage:{
        width:constant.resWidth(60),
        height:constant.resWidth(60),
    },
    registerInputView:{
        flexDirection:'row',
        marginHorizontal:'5%',
        borderRadius:10,
        alignItems:'center',
        borderWidth:1.5,
        marginBottom:'5%',
        paddingVertical:'0.5%',
        borderColor:constant.grayColor
        } ,
       
        registerTextInput:{
      flex:1,
      fontSize:constant.font14,
      marginHorizontal:'5%',
      borderRadius:10,
      alignItems:'center',
      borderWidth:1.5,
      marginBottom:'5%',
    //   paddingVertical:'0.5%',
      borderColor:constant.grayColor,
      paddingLeft:"3%"
        },

        //otpScreen
        otpScreenMainView:{
            backgroundColor:constant.whiteColor,
            flex:1
        },
        otpScreenHeaderButton:{
        //  backgroundColor:'red',
         alignSelf:'flex-start',
         padding:"3%"
        },
        otpScreenHeaderImage:{
            width:constant.resWidth(10),
            height:constant.resWidth(10),
        },
        otpTextInput:{
            fontSize:constant.font14,
            marginHorizontal:'5%',
            borderRadius:10,
            alignItems:'center',
            borderWidth:1.5,
            marginBottom:'5%',
            borderColor:constant.grayColor,
            paddingLeft:"3%",
            textAlign:'center'
        },
        otpScreenImage:{
            
        width:constant.resWidth(80),
        height:constant.resWidth(40),
        alignSelf:'center',
        marginTop:'25%',
        marginBottom:'20%'
        },
        otpScreenText:{
         marginHorizontal:'5%',
         marginBottom:'5%',
         fontSize:constant.font10,
         color:constant.grayColor
        },
        otpScreenText2:{
            marginHorizontal:'5%',
            marginBottom:'5%',
            fontSize:constant.font14,
            color:constant.baseColor
           },
           otpScreenResendText:{
            marginHorizontal:'5%',
            paddingVertical:"1.5%",
            fontSize:constant.font12,
            color:constant.baseColor,
            alignSelf:'flex-start'
         
           },

//Home
HomeBloodListMainView:{
    flexDirection:"row",
    width:constant.resWidth(42),
    height:constant.resWidth(17),
    // borderWidth:2,
    borderRadius:8,
    marginBottom:constant.resWidth(6),
    // borderColor:constant.baseColor,
    alignItems:'center',
    justifyContent:'space-between',
    backgroundColor:'#F5F5F5',
    paddingHorizontal:constant.resWidth(6)
},
homeBloodListImage:{
    width:constant.resWidth(8),
    height:constant.resWidth(8),
    
},
homeTitleText:{
 fontSize:constant.font20,
 color:constant.blackColor,
 fontWeight:'700'
},

//Search
SearchListMainView:{
    // width:constant.resWidth(32),
    // height:constant.resWidth(30),
    // borderWidth:2,
    borderRadius:10,
    marginBottom:constant.resWidth(4),
    // borderColor:constant.baseColor,
    alignItems:'center',
    // justifyContent:'center',
    backgroundColor:'#F5F5F5',
    marginHorizontal:'5%',
    flexDirection:'row',
    elevation:2
},
searchListImage:{
    width:constant.resWidth(15),
    height:constant.resWidth(15),
    borderRadius:constant.resWidth(40),
    marginHorizontal:constant.resWidth(3),
    borderColor:constant.whiteColor,
    borderWidth:2,
    
},
SearchListSubView:{
flexDirection:'row',
alignItems:"center",
flex:1,
paddingVertical:'3%'
},
searchListImageMainView:{
    flex:1
},
searchListName:{
color:constant.blackColor,
fontSize:constant.font16,
},

searchListAddress:{
    color:constant.blackColor,
    fontSize:constant.font8/1.2,
    },
    searchPhone:{

        width:constant.resWidth(8),
        height:constant.resWidth(8),
        tintColor:constant.greenColor
    },
    searchPhoneButton:{
    flex:0.25,
    justifyContent:'center',
    alignItems:'center',

    },

    //Profile

    ProfileUserImage:{
        width:constant.resWidth(20),
        height:constant.resWidth(20),
    },
    profileImageButton:{
        width:constant.resWidth(30),
        height:constant.resWidth(30),
        alignSelf:'center',
        borderWidth:1.5,
        borderRadius:constant.resWidth(20),
        borderColor:constant.baseColor,
        marginVertical:constant.resWidth(15),
        alignItems:'center',
        justifyContent:'center',
    },
})

export default styles
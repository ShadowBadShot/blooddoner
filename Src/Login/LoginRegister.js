import React from 'react'
import {View,Image,StatusBar}  from 'react-native'
import styles from '../Utils/MainStyles'
import * as constant from '../Utils/Constant'
import CommonButton from '../Component/Button'
import {CommonActions} from '@react-navigation/native';



const LoginRegister =(props)=>{


    const onLogin = ()=>{
        props.navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [
                { name: 'Login' },
                
              ],
            }))
    }
    const onRegister = ()=>{
        props.navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [
                { name: 'Register' },
                
              ],
            }))
    }


    return(
        <View style={styles.LoginRegisterMainView}>
            <StatusBar  />
       <Image 
       source={require('../Resource/Images/dropImage.png')}
       resizeMode='contain' style={styles.LoginRegiterImage}
       />
     <CommonButton 
     onPress={()=>{onLogin()}}
    title='LOGIN'
     buttonStyleExt={{
    backgroundColor:'#ffffff',
    width:'90%',
    alignSelf:'center',
    marginTop:constant.resWidth(45)

     }}
     titleStyleExt={{
        color:constant.baseColor,
        fontWeight:'800',
        fontSize:constant.font20
     }}
     />

<CommonButton 
     onPress={()=>{onRegister()}}
    title='REGISTER'
     buttonStyleExt={{
    width:'90%',
    alignSelf:'center',
    marginTop:constant.resWidth(5),
    borderWidth:2,
    borderColor:constant.whiteColor,
    height:constant.resWidth(13),
    marginBottom:constant.resWidth(10)

     }}
     titleStyleExt={{
        color:constant.whiteColor,
        fontWeight:'700',
        fontSize:constant.font18
     }}
     />

        </View>
    )
}

export default LoginRegister
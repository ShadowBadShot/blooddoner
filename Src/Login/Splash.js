import React,{useEffect,} from 'react'
import {View,Image,StatusBar,Text}  from 'react-native'
import { CommonActions } from '@react-navigation/native';
import * as constant from '../Utils/Constant'
const Splash =(props)=>{

    useEffect(()=>{
     setTimeout(()=>{
        props.navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [{ name: 'Intro' }],
            }),
          );
     },1000)
    },[])
    return(
        <View style={{flex:1,backgroundColor:constant.whiteColor,justifyContent:'center',alignItems:'center'}}>
        <StatusBar translucent backgroundColor={'transparent'} />
        <Text 
       style={{
           fontSize:1.2*constant.font20,
           color:constant.baseColor,
           fontWeight:'bold',
           marginBottom:'5%'
        }}
       >DONATE  BLOOD</Text>
       <Image 
       source={require('../Resource/Images/splashImage.png')}
       resizeMode='cover'
        style={{
            height:300,
            width:200,
        }}
       />
       <Text 
       style={{
           fontSize:1.2*constant.font20,
           color:constant.baseColor,
           fontWeight:'bold',
           marginTop:'5%'
        }}
       >SAVE  LIFE</Text>
        </View>
    )
}

export default Splash
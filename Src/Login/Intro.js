import React from 'react'
import {View,Image,StatusBar,Pressable,Text}  from 'react-native'
import styles from '../Utils/MainStyles'
import * as constant from '../Utils/Constant'
import CommonButton from '../Component/Button'
import {CommonActions} from '@react-navigation/native';



const Intro =(props)=>{
    return(
        <View style={styles.IntroMainView}>
            <StatusBar translucent  backgroundColor='transparent' />
       <Image 
       source={require('../Resource/Images/dropImage.png')}
       resizeMode='contain' style={styles.introMainImage}
       />
    
    <View style={styles.introSubView}>
    <Pressable  style={styles.IntroButton} onPress={()=>{props.navigation.navigate("LoginRegister")}}>
     <Image source={require('../Resource/Images/UserImage.png')} style={styles.introImageStyle} />
     <Text style={styles.introTitle}>DONOR</Text>
</Pressable>
<Pressable  style={styles.IntroButton} onPress={()=>{props.navigation.navigate("Home")}}>
     <Image source={require('../Resource/Images/UserImage.png')} style={styles.introImageStyle} />
     <Text style={styles.introTitle}>FINDER</Text>
</Pressable>
    </View>
     

        </View>
    )
}

export default Intro
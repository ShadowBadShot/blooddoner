import React,{useRef,useState,useEffect} from 'react'
import {View,Image,StatusBar,Text,TextInput,ScrollView}  from 'react-native'
import styles from '../Utils/MainStyles'
import * as constant from '../Utils/Constant'
import CommonButton from '../Component/Button'
import {CommonActions} from '@react-navigation/native';
import {Phone,lock} from '../Utils/Images'


const Login =(props)=>{
const scrollRef = useRef(null)
const [phone,setPhone] = useState('')
const [pass,setPass] = useState('')

    const onLogin = ()=>{
    if(phone==='') constant.showAlert("Please enter mobile number",false)
   else if(phone.length < 10) constant.showAlert("Please enter valid mobile number",false)
    else if(pass==='') constant.showAlert("Please enter password",false)
    else{
 props.navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [
                { name: 'Home' },
                
              ],
            }))
    }
       
    }
  


    return(
       <ScrollView ref={scrollRef}
            style={styles.loginSubView}>
           <StatusBar backgroundColor={constant.baseColor} />
<View >
       <Image 
       source={require('../Resource/Images/login1.jpeg')}
       resizeMode='cover' style={styles.LoginImage}
       />
       </View>
       <View style={{marginTop:constant.resWidth(20)}}>
       <View style={[styles.detailMainView,{marginBottom:'5%'}]}>
           <Image source={Phone} style={styles.loginPhoneImage} />
           <TextInput 
           style={styles.loginTextInput}
            placeholder='Phone Number'
            keyboardType='numeric'
            onChangeText={(text)=>{setPhone(text)}}
            maxLength={10}
            onFocus={()=>scrollRef.current.scrollToEnd()}
             ></TextInput>
       </View>
       <View style={[styles.detailMainView,{marginBottom:'0%'}]}>
           <Image source={lock} style={styles.loginPhoneImage} />
           <TextInput 
           style={styles.loginTextInput}
            placeholder='Password'
            onChangeText={(text)=>{setPass(text)}}
            secureTextEntry={true}
            onFocus={()=>scrollRef.current.scrollToEnd()}
             ></TextInput>
       </View>
       <Text style={styles.loginForgetPassword} onPress={()=>{props.navigation.navigate("ChangePassword")}}>Forgot Password</Text>
       
     <CommonButton 
     onPress={()=>{onLogin()}}
    title='LOGIN'
     buttonStyleExt={{
    backgroundColor:constant.baseColor,
    width:'90%',
    alignSelf:'center',
    marginBottom:constant.resWidth(2),

     }}
     titleStyleExt={{
        color:constant.whiteColor,
        fontWeight:'800',
        fontSize:constant.font20
     }}
     />
</View>
 <Text  onPress={()=>{props.navigation.navigate("Register")}}
 style={styles.loginBottomText}>New User? <Text style={styles.loginBottomText2}>Create Account</Text></Text>
 </ScrollView>
    )
}

export default Login
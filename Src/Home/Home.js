import React from 'react'
import { View, FlatList, Image,StatusBar,Pressable,Text} from 'react-native'
import * as constant from '../Utils/Constant'
import HomeHeader from '../Component/HomeHeader'
import Carousel from 'react-native-banner-carousel-updated';
import * as ApiConfig from '../Utils/ApiConfig'
import styles from '../Utils/MainStyles'
const images = [
    {"key":1,"image":require('../Resource/Images/Banner1.jpeg')},
    {"key":2,"image":require('../Resource/Images/Banner2.png')},
    {"key":3,"image":require('../Resource/Images/Banner2.png')},

];

const bloodList =[
  {"key":1,"image":require('../Resource/Images/blood.png'),'Title':'A+'},
  {"key":2,"image":require('../Resource/Images/blood.png'),'Title':'A-'},
  {"key":3,"image":require('../Resource/Images/blood.png'),'Title':'B+'},
  {"key":4,"image":require('../Resource/Images/blood.png'),'Title':'B-'},
  {"key":5,"image":require('../Resource/Images/blood.png'),'Title':'O+'},
  {"key":6,"image":require('../Resource/Images/blood.png'),'Title':'O-'},
  {"key":7,"image":require('../Resource/Images/blood.png'),'Title':'AB+'},
  {"key":8,"image":require('../Resource/Images/blood.png'),'Title':'AB-'},


]
const Home =(props)=> {

  function renderPage(item, index) {
  
        return (
            <View key={index} >
                <Image
                 style={{ 
                   width: constant.resWidth(90),
                   height: constant.resWidth(45),
                   alignSelf:'center',
                   borderRadius:constant.resWidth(3)
                  }} 
                 source={item.image}
                 resizeMode='contain'
                 />
            </View>
        );
    }

    return (
      <View style={{flex: 1,backgroundColor:constant.whiteColor,marginTop:StatusBar.currentHeight}}  >
        <StatusBar translucent backgroundColor={constant.baseColor} />
         <HomeHeader 
          title="Search"
          leftIcon={require("../Resource/Images/UserImage.png")}
          bgColor={constant.baseColor}
          leftIconPress={() => props.navigation.navigate("Profile")}
          rightExt={{tintColor:constant.whiteColor,borderRadius:10,}}
          leftButtonExt={{  borderColor:constant.whiteColor,borderWidth:1,borderRadius:constant.resWidth(20),padding:'3%'
          }}

         />
            <View style={{
            backgroundColor:constant.baseColor,
            paddingBottom:constant.resWidth(5),
            paddingTop:constant.resWidth(4)
            // marginTop:1
            }}>
            <Carousel
                    autoplay
                    autoplayTimeout={30000}
                    loop
                    index={0}
                    pageSize={constant.deviceWidth}
                    showsPageIndicator={false}
                >
                    {images.map((item, index) => renderPage(item, index))}
                </Carousel>
            </View>
            <FlatList 
            style={{marginTop:constant.resWidth(10),marginHorizontal:'5%'}}
            data={bloodList}
            numColumns={2}
            contentContainerStyle={{}}
            columnWrapperStyle={{justifyContent:'space-between'}}
            renderItem={({item,index})=>{
              return(
                <Pressable onPress={()=>{props.navigation.navigate("Search")}} style={styles.HomeBloodListMainView}>
                <Text style={styles.homeTitleText}>{item.Title}</Text>
                 <Image
                 source={item.image}
                 resizeMode='contain'
                 style={styles.homeBloodListImage}
                 />
                  </Pressable>
              )
            }}
            />
           
            
           
      </View>

    )
  
}
export default Home

import React,{useRef,useState,useEffect} from 'react'
import {View,Image,StatusBar,Text,TextInput,ScrollView}  from 'react-native'
import styles from '../Utils/MainStyles'
import * as constant from '../Utils/Constant'
import CommonButton from '../Component/Button'
import {CommonActions} from '@react-navigation/native';
import LoaderModal from '../Component/LoaderModal'
import {BackArrow} from '../Utils/Images'


const Login =(props)=>{
const [Otp,setOtp] = useState('')
const [modalVisible,setModalVisible] = useState(false)

    const onSubmit = ()=>{
    if(Otp==='') constant.showAlert("Please enter Otp",false)
   else if(Otp.length < 3) constant.showAlert("Please enter valid Otp",false)
    else{
 props.navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [
                { name: 'Home' },
                
              ],
            }))
    }
       
    }
  


    return(

       <View 
            style={styles.otpScreenMainView}>
                {/* <Pressable style={styles.otpScreenHeaderButton}>
                    <Image source={BackArrow} style={styles.otpScreenHeaderImage} resizeMode='contain' />
                </Pressable> */}
           <StatusBar backgroundColor={constant.baseColor} />
<View >
       <Image 
       source={require('../Resource/Images/login1.jpeg')}
       resizeMode='cover' style={styles.otpScreenImage}
       />
       </View>
      <Text style={styles.otpScreenText}>We have send an OTP on  <Text style={styles.otpScreenText2}> +91-7404814959</Text></Text>
           <TextInput 
            style={styles.otpTextInput}
            placeholder='OTP'
            keyboardType='numeric'
            onChangeText={(text)=>{setOtp(text)}}
            maxLength={5}
             ></TextInput>
     
     <CommonButton 
     onPress={()=>{onSubmit()}}
    title='SUBMIT'
     buttonStyleExt={{
    backgroundColor:constant.baseColor,
    width:'90%',
    alignSelf:'center',
    marginBottom:constant.resWidth(1),
    marginTop:constant.resWidth(6)

     }}
     titleStyleExt={{
        color:constant.whiteColor,
        fontWeight:'500',
        fontSize:constant.font18
     }}
     />
     <Text onPress={()=>{setModalVisible(true)}} style={styles.otpScreenResendText}>Resend OTP</Text>
     <LoaderModal isVisible={modalVisible} onRequestClose={()=>{setModalVisible(false)}} />
</View>
    )
}

export default Login
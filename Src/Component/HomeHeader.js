import React from "react"
import { View, Text, StyleSheet, Image, Platform,StatusBar,TouchableOpacity,TextInput  } from "react-native"
import * as constant from "../Utils/Constant"


const Header=({
  leftIcon,
  title,
  rightIcon,
  rightExt,
  bgColor,
  leftIconPress,
  leftButtonExt
})=>{
  return(
    <View>
     
      { Platform.OS=="ios" && <View style={{height: 35, backgroundColor: bgColor}} />}
      <View style={{flexDirection: "row", height: 55, alignItems: "center", backgroundColor: bgColor}}>
    <StatusBar backgroundColor={bgColor} />       
        <View style={{flex: 0.5, alignItems: "center",flexDirection:'row',marginHorizontal:'5%' }}>
        { leftIcon !== null &&
        <TouchableOpacity style={[leftButtonExt]}  onPress={()=>leftIconPress()}>
            <Image source={leftIcon} style={[styles.rightIconStyle, rightExt]} />
            </TouchableOpacity>
          }
          <Text style={styles.title} onPress={()=>leftIconPress()}>Hii Jogi</Text>
        </View>
       
       
      </View>
    </View>
  )
}

export default Header

Header.defaultProps= {
  leftIcon: null,
  title: "",
  rightIcon: null,
  rightExt: null,
  leftButtonExt:null,
  bgColor: "transparent",
  leftIconPress:function(){}
}

const styles = StyleSheet.create({
  title: {
    color: constant.whiteColor,
    fontSize: constant.font16,
    // fontWeight: "500",
    marginLeft:'5%'
  },
  rightIconStyle: {
    height: 30,
    width: undefined,
    aspectRatio: 1/1,
    backgroundColor: "transparent"
  }
})
import React from "react"
import { View, Text, StyleSheet, Image, Platform,StatusBar,TouchableOpacity  } from "react-native"
import * as constant from "../Utils/Constant"


const Header=({
  leftIcon,
  title,
  rightIcon,
  rightExt,
  bgColor,
  leftIconPress,
  leftButtonExt
})=>{
  return(
    <View>
     
      { Platform.OS=="ios" && <View style={{height: 35, backgroundColor: bgColor}} />}
      <View style={{flexDirection: "row", height: 55, alignItems: "center", backgroundColor: bgColor}}>
    <StatusBar backgroundColor={bgColor} />
        
        <View style={{flex: 0.5, alignItems: "center" }}>
        { leftIcon !== null &&
        <TouchableOpacity style={[leftButtonExt]} onPress={()=>leftIconPress()}>
            <Image source={leftIcon} style={[styles.rightIconStyle, rightExt]} />
            </TouchableOpacity>
          }
        </View>
        <View style={{flex: 2, alignItems: "center"}}>
          <Text style={styles.title}>{title}</Text>
        </View>
        <View style={{flex: 0.5}}>
          { rightIcon !== null &&(
            <TouchableOpacity onPress={()=>leftIconPress()}>
            <Image source={rightIcon} style={[styles.rightIconStyle, rightExt]} />
            </TouchableOpacity>
           ) }
        </View>
      </View>
    </View>
  )
}

export default Header

Header.defaultProps= {
  leftIcon: null,
  title: "",
  rightIcon: null,
  rightExt: null,
  leftButtonExt:null,
  bgColor: "transparent",
  leftIconPress:function(){}
}

const styles = StyleSheet.create({
  title: {
    color: constant.whiteColor,
    fontSize: constant.font18,
    fontWeight: "700"
  },
  rightIconStyle: {
    height: 30,
    width: undefined,
    aspectRatio: 1/1,
    backgroundColor: "transparent"
  }
})
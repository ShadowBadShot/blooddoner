import React from 'react';
import {Modal,View,ActivityIndicator} from 'react-native';
import  * as constant from '../Utils/Constant';

const LoaderModal = ({isVisible,onRequestClose}) => {
    return (
      <Modal visible={isVisible}  onRequestClose={onRequestClose} transparent>
          <View  style={{flex:1,backgroundColor:'#59595980',alignItems:'center',justifyContent:'center'}}>
          <ActivityIndicator size='large' color={constant.baseColor} />
       </View>
      </Modal>
    );
  };

  LoaderModal.defaultProps={
    
    isVisible: false,
    onRequestClose: function () { },
  }
  
  
  export default LoaderModal;

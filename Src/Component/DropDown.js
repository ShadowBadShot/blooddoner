import React, {useState, useEffect} from 'react';
import {StyleSheet, Image,ActivityIndicator,Pressable,Text} from 'react-native';
import * as constant from '../Utils/Constant';
import { Dropdown } from 'react-native-element-dropdown';

const DropDown = ({
  data,
  value,
  onSelect,
  dropDownStyleExt,
  placeholder
}) => {
  return (
    <Dropdown
    style={[styles.dropdown,dropDownStyleExt]}
    // placeholderStyle={styles.placeholderStyle}
    selectedTextStyle={styles.selectedTextStyle}
    // inputSearchStyle={styles.inputSearchStyle}
    // iconStyle={styles.iconStyle}
    data={data}
    search
    maxHeight={300}
    labelField="label"
    valueField="value"
    placeholder={placeholder}
    searchPlaceholder="Search..."
    value={value}
    onChange={item => {
      onSelect(item.value);
    }}
  
  />
  );
};
export default DropDown;

const styles = StyleSheet.create({
    dropdown:{
        marginHorizontal:'5%',
        borderWidth:1,
        borderRadius:10,
        paddingHorizontal:'3%',
        paddingVertical:'1.5%',
        
    },
    selectedTextStyle:{
        fontSize:constant.font14,
        color:constant.blackColor
    }
});

DropDown.defaultProps = {

};



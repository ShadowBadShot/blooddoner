import React, {useState, useEffect} from 'react';
import {StyleSheet, Image,ActivityIndicator,Pressable,Text} from 'react-native';
import * as constant from '../Utils/Constant';

const CommonButton = ({
  buttonStyleExt,
  titleStyleExt,
  title,
  onPress,
  loaderVisible,
  buttonDisable
}) => {
  return (
    <Pressable style={[styles.buttonStyle,buttonStyleExt]} onPress={()=>{onPress()}}>
    
      {
    loaderVisible ?  <ActivityIndicator size='large' color={constant.whiteColor} animating={loaderVisible} />
      : null
      }

    <Text style={[
      styles.titleStyle,
       titleStyleExt,
      {marginLeft:loaderVisible ? constant.resWidth(5):'0%',
       marginRight:loaderVisible ? constant.resWidth(5):'0%'
      }]}>{title}</Text>
    {

    loaderVisible ?  <ActivityIndicator size='large' color={constant.whiteColor} animating={false} />
    :null
}

    </Pressable>
  );
};
export default CommonButton;

const styles = StyleSheet.create({
  buttonStyle: {
    height: constant.resWidth(12.5),
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: constant.borderRadius1,
    marginBottom: constant.footerSpace,
    backgroundColor: constant.baseColor,
    flexDirection:'row'
  },
  titleStyle: {
    fontFamily: constant.typeMedium,
    fontSize: constant.font14,
    color: constant.whiteColor,
    // marginRight :constant.resWidth(5),
    // marginLeft:constant.resWidth(5),
  },
});

CommonButton.defaultProps = {
  buttonImagePath: null,
  loaderVisible:false,
  buttonDisable:false
};



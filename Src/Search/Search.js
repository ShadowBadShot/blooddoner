import React,{useState} from 'react'
import { View, FlatList, Image,StatusBar,TextInput,Text,Pressable,Linking,Platform} from 'react-native'
import * as constant from '../Utils/Constant'
import Header from '../Component/Header'
import Carousel from 'react-native-banner-carousel-updated';
import * as ApiConfig from '../Utils/ApiConfig'
import styles from '../Utils/MainStyles'
import DropDown from '../Component/DropDown'

const bloodList =[
  {"key":1,"name":'Jogi',"State":'India',"City":"Rohtak","PhoneNo":8168030276},
  {"key":2,"name":'Jogi',"State":'India',"City":"Rohtak","PhoneNo":8168030276},
  {"key":3,"name":'Jogi',"State":'India',"City":"Rohtak","PhoneNo":8168030276},
  {"key":4,"name":'Jogi',"State":'India',"City":"Rohtak","PhoneNo":8168030276},
  {"key":5,"name":'Jogi',"State":'India',"City":"Rohtak","PhoneNo":8168030276},
  {"key":6,"name":'Jogi',"State":'India',"City":"Rohtak","PhoneNo":8168030276},
  {"key":7,"name":'Jogi',"State":'India',"City":"Rohtak","PhoneNo":8168030276},
  {"key":8,"name":'Jogi',"State":'India',"City":"Rohtak","PhoneNo":8168030276},


]

const data = [
  { label: 'Hissar', value: 'Hissar' },
  { label: 'Jind', value: 'Jind' },
  { label: 'Rohtak', value: 'Rohtak' },
  { label: 'Sirsa', value: 'Sirsa' },

];
const Search =(props)=> {
  const [city,setCity] = useState('')

    const callNumber = phone => {
        console.log('callNumber ----> ', phone);
        let phoneNumber = phone;
        if (Platform.OS !== 'android') {
          phoneNumber = `telprompt:${phone}`;
        }
        else  {
          phoneNumber = `tel:${phone}`;
        }
        Linking.openURL(phoneNumber);
        // Linking.canOpenURL(phoneNumber)
        // .then(supported => {
            
        //   if (!supported) {
        //     alert('Phone number is not available');
        //   } else {
        //     return Linking.openURL(phoneNumber);
        //   }
        // })
        // .catch(err => console.log(err));
      };


    return (
      <View style={{flex: 1,backgroundColor:constant.whiteColor,paddingTop:StatusBar.currentHeight}} >
       
 <Header
          title="Search"
          leftIcon={require("../Resource/Images/backIcon.png")}
          bgColor={constant.baseColor}
          leftIconPress={() => props.navigation.goBack()}
          rightExt={{tintColor:constant.whiteColor}}

          /> 
          <DropDown 
   placeholder='Select City'
   data={data}
   value={city}
   onSelect = {(text)=>{setCity(text)}}
   dropDownStyleExt={{marginVertical:'5%',borderBottomWidth:1,borderWidth:0,borderRadius:0 }}

   />
            <FlatList 
            data={bloodList}
            renderItem={({item,index})=>{
              return(
                <View style={styles.SearchListMainView}>
                    <View style={styles.SearchListSubView}>
                  <Image source={require('../Resource/Images/UserImage.png')} style={styles.searchListImage} />
                  <View style={styles.searchListImageMainView}>
                  <Text style={styles.searchListName} >{item.name}</Text>
                  <Text style={styles.searchListAddress} >{item.City} {item.State}</Text>

                </View>
                  </View>
                   <Pressable style={styles.searchPhoneButton} onPress={()=>callNumber(item.PhoneNo)} >
                       <Image source={require("../Resource/Images/phoneIcon.png")} style={styles.searchPhone} />
                   </Pressable>
                  </View>
              )
            }}
            />
     </View>

    )
  
}
export default Search
